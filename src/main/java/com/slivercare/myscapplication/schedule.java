package com.slivercare.myscapplication;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by linchingyan on 2016/8/26.
 */

public class schedule extends Activity {


    TextView user_id,user_user_id,user_type,user_time,user_service_id,user_process,user_note;

    Button finish_button;

    String pid;

    // Progress Dialog
    private ProgressDialog pDialog;

    // Creating JSON Parser object
    JSONParser jsonParser = new JSONParser();

    // single product url
    private static final String url_get = Url.url_get_task_detail;

    // JSON Node names
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_PRODUCT = "product";

    private static final String TAG_ID = "id";
    private static final String TAG_USER_ID = "user_id";
    private static final String TAG_TYPE = "type";
    private static final String TAG_TIME = "time";
    private static final String TAG_SERVICE_ID = "service_id";
    private static final String TAG_NOTE = "note";
    private static final String TAG_PROCESS = "process";
    private static final String TAG_START_TIME = "start_time";



    String getText1,getText2,getText3,getText4,getText5,getText6,getText7,getText8;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.schedule);

        // save button
        finish_button = (Button) findViewById(R.id.finish_button);

        user_id = (TextView) findViewById(R.id.user_id);
        user_user_id = (TextView) findViewById(R.id.user_user_id);
        user_type = (TextView) findViewById(R.id.user_type);
        user_time = (TextView) findViewById(R.id.user_time);
        user_service_id = (TextView) findViewById(R.id.user_service_id);
        user_note = (TextView) findViewById(R.id.user_note);
        user_process = (TextView) findViewById(R.id.user_process);


        // getting product details from intent
        Intent i = getIntent();


        // getting product id (pid) from intent
//        pid = i.getStringExtra(TAG_ID);

        // Getting complete product details in background thread
        new GetProductDetails().execute();

        // save button click event
        finish_button.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                String id = user_id.getText().toString();
                String user_id = user_user_id.getText().toString();
                String type = user_type.getText().toString();
                String time = user_time.getText().toString();
                String service_id = user_service_id.getText().toString();
                String note = user_note.getText().toString();
                String process = user_process.getText().toString();

                new ReceiveTask().execute(id, user_id, type, time, service_id, note, process);

                // starting background task to update product
                //new SaveProductDetails().execute();
            }
        });

        // Delete button click event
//        btnDelete.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View arg0) {
//                // deleting product in background thread
//                new DeleteProduct().execute();
//            }
//        });

    }
    /**
     * Background Async Task to Get complete product details
     * */
    public void gotoback(View v){
        Intent it = new Intent(this, MainActivity.class);
        startActivity(it);

    }
    class GetProductDetails extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(schedule.this);
            pDialog.setMessage("取得任務詳細資料...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        /**
         * Getting product details in background thread
         * */
        protected String doInBackground(String... params) {

            // updating UI from Background Thread

            // Check for success tag
//            int success;
//            String success;
            int success;
            try {
                // Building Parameters
                List<NameValuePair> gparams = new ArrayList<>();

//                        gparams.add(new BasicNameValuePair("pid", pid));
                gparams.add(new BasicNameValuePair("id", "1"));

                // getting product details by making HTTP request
                // Note that product details url will use GET request
                JSONObject json = jsonParser.makeHttpRequest(url_get, "GET", gparams);

                // check your log for json response
                Log.d("Single Product Details", json.toString());

                // json success tag
//                success = json.getString(TAG_SUCCESS);
                success = json.getInt(TAG_SUCCESS);
//                success.equals("1")
//                success ==1
                if (success ==1) {
                    // successfully received product details
                    JSONArray productObj = json.getJSONArray(TAG_PRODUCT); // JSON Array

                    // get first product object from JSON Array
                    JSONObject product = productObj.getJSONObject(0);

                    // product with this pid found
                    // Edit Text

                    // display product data in EditText

                    getText1 = product.getString(TAG_ID);
                    getText2 = product.getString(TAG_USER_ID);
                    getText3 = product.getString(TAG_TYPE);
                    getText4 = product.getString(TAG_TIME);
                    getText5 = product.getString(TAG_SERVICE_ID);
                    getText6 = product.getString(TAG_NOTE);
                    getText7 = product.getString(TAG_PROCESS);
                    getText8 = product.getString(TAG_START_TIME);

                }
                else{
                    getText1 = "-1";
                    getText2 = "-1";
                    getText3 = "-1";
                    getText4 = "-1";
                    getText5 = "-1";
                    getText6 = "-1";
                    getText7 = "-1";
                    getText8 = "-1";
                    // product with pid not found
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }


            return null;
        }


        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            // dismiss the dialog once got all details
            if(Integer.valueOf(getText1)==-1){
                getText1 = "暫無資料";
                getText2 = "1";
                getText4 = "暫無資料";
                getText5 = "暫無資料";
                getText6 = "暫無資料";}

            if(Integer.valueOf(getText3)==1)
                getText3 = "緊急聯絡";
            else if(Integer.valueOf(getText3)==2)
                getText3 = "盥洗";
            else if(Integer.valueOf(getText3)==3)
                getText3 = "外出";
            else if(Integer.valueOf(getText3)==4)
                getText3 = "餐飲";
            else if(Integer.valueOf(getText3)==5)
                getText3 = "清潔";
            else if(Integer.valueOf(getText3)==-1)
                getText3 = "暫無資料";

            if(Integer.valueOf(getText7)==0)
                getText7 = "未接受";
            else if(Integer.valueOf(getText7)==1)
                getText7 = "已接受";
            else if(Integer.valueOf(getText7)==2)
                getText7 = "處理中";
            else if(Integer.valueOf(getText7)==3)
                getText7 = "已完成";
            else if(Integer.valueOf(getText7)==-1)
                getText7 = "暫無資料";

            user_id.setText(getText1);
            user_user_id.setText(getText2);
            user_type.setText(getText3);
            user_time.setText(getText4);
            user_service_id.setText(getText5);
            user_note.setText(getText6);
            user_process.setText(getText7);

            pDialog.dismiss();
        }
    }

    /**
     * Background Async Task to  Save product Details
     * */
    class ReceiveTask extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(schedule.this);
            pDialog.setMessage("任務完成...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        /**
         * Saving product
         * */
        protected String doInBackground(String... args) {

            // getting updated data from TextView 接資料從執行緒
            String id = args[0];
//                    user_id = args[1],
//                    description = args[2];

            SimpleDateFormat sDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.TAIWAN);
            String date = sDateFormat.format(new java.util.Date());

            Log.d("TIME", date);

            //只需上傳 Worker id 、 get_time
            // Building Parameters
            List<NameValuePair> params = new ArrayList<>();
            if(!getText8.equals("0000-00-00 00:00:00") && !getText8.equals("-1")){
                params.add(new BasicNameValuePair("id", id));
//            params.add(new BasicNameValuePair("worker_id", "1"));
                params.add(new BasicNameValuePair("finish_time", date));
                params.add(new BasicNameValuePair("process", "3"));
            }

            // sending modified data through http request
            // Notice that update product url accepts POST method
            JSONObject json = jsonParser.makeHttpRequest(Url.url_receive_task, "POST", params);

            // check json success tag
            try {
                int success = json.getInt(TAG_SUCCESS);

                if (success == 1) {
                    // successfully updated
                    Intent i = getIntent();
                    // send result code 100 to notify about product update
                    setResult(100, i);
//                    finish();
                } else {
                    // failed to update product
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }


        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {

            AlertDialog.Builder MeAlerDialog = new AlertDialog.Builder(schedule.this);
            if(!getText8.equals("0000-00-00 00:00:00") && !getText8.equals("-1"))
                MeAlerDialog.setTitle("服務已經完成");
            else
                MeAlerDialog.setTitle("服務尚未開始");
            DialogInterface.OnClickListener OKclick = new DialogInterface.OnClickListener()
            {
                public void onClick(DialogInterface dialog, int which) {
                    Intent it = new Intent(schedule.this, MainActivity.class);
                    startActivity(it);
                    finish();
                }
            };
            if(!getText8.equals("0000-00-00 00:00:00") && !getText8.equals("-1"))
                MeAlerDialog.setNeutralButton("確認",OKclick);
            else
                MeAlerDialog.setNeutralButton("返回",OKclick);
            MeAlerDialog.show();
            pDialog.dismiss();


        }

    }


}
