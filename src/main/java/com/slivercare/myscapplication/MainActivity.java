package com.slivercare.myscapplication;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.KeyEvent;
import android.view.Menu;
import android.widget.Button;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;




public class MainActivity extends Activity {
//    private TextView textView;
    Button btn=null;
    int btn_num;

    // Progress Dialog
    private ProgressDialog pDialog;
    JSONParser jsonParser = new JSONParser();
    // url to create
    private static String url_create_task = "http://125.227.141.117:16817/create_task.php";
//   "http://project2135.esy.es/sc_connect/create_task.php"
//   "http://125.227.141.117:16817/create_task.php"

    // JSON Node names
    private static final String TAG_SUCCESS = "success";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.i("--Main--", "onCreate");

    }

    public void gotonext(View v){
        Intent it = new Intent(this, schedule.class);
        startActivity(it);


    }

    public void event(View v){
        switch (v.getId()) {
            case R.id.button1:
                btn_num = 1;
                new CreateEvent().execute();
                break;
            case R.id.button2:
                btn_num = 2;
                new CreateEvent().execute();
                break;
            case R.id.button3:
                btn_num = 3;
                new CreateEvent().execute();
                break;
            case R.id.button4:
                btn_num = 4;
                new CreateEvent().execute();
                break;
            case R.id.button5:
                btn_num = 5;
                new CreateEvent().execute();
                break;

        }


    }

    /**
     * Background Async Task to Create new product
     * */
    class CreateEvent extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(MainActivity.this);
            pDialog.setMessage("傳送中..");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        /**
         * Creating product
         * */
        protected String doInBackground(String... args) {

            // Building Parameters
            List<NameValuePair> params = new ArrayList<>();
            params.add(new BasicNameValuePair("user_id", "1"));
            params.add(new BasicNameValuePair("type", String.valueOf(btn_num)));

            // getting JSON Object
            // Note that create product url accepts POST method
            JSONObject json = jsonParser.makeHttpRequest(url_create_task,
                    "POST", params);

            // check log cat fro response
//            Log.d("Create Response", json.toString());

            // check for success tag
            /*
            try {
                int success = json.getInt(TAG_SUCCESS);

                if (success == 1) {
                    // successfully created product
                    Intent i = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(i);

                    // closing this screen
                    finish();
                } else {
                    // failed to create product
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            */

            return null;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            // dismiss the dialog once done
//            new AlertDialog.Builder(this)
//                    .setMessage("確認增加？")
//                    .setPositiveButton("確認")
//                    .setNegativeButton("取消")

            AlertDialog.Builder MyAlerDialog = new AlertDialog.Builder(MainActivity.this);
            MyAlerDialog.setTitle("事件已增加完成");
//            MyAlerDialog.setMessage("請按下離開鍵");
            DialogInterface.OnClickListener OKclick = new DialogInterface.OnClickListener()
            {
                public void onClick(DialogInterface dialog, int which) {

                }
            };
            MyAlerDialog.setNeutralButton("確認",OKclick);
            MyAlerDialog.show();
            pDialog.dismiss();
        }

    }


}//MainActivity